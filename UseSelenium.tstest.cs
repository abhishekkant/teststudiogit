using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;

namespace DropDownsTestStudio
{

    public class UseSelenium : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        [CodedStep(@"New Coded Step")]
        public void UseSelenium_CodedStep()
        {
             IWebDriver driver = new ChromeDriver(@"C:\selenium-dotnet-2.53.1");
            //IWebDriver driver = new InternetExplorerDriver(@"C:\selenium-dotnet-2.53.1");
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/login");
            var userName = driver.FindElement(By.Id("username"));
            var passWord = driver.FindElement(By.Id("password"));
            var loginButton = driver.FindElement(By.XPath("//*[@id=\"login\"]/button/i"));
            
            userName.SendKeys("tomsmith");
            passWord.SendKeys("SuperSecretPassword!");
            
            loginButton.Click();
            
           var loginMessage = driver.FindElement(By.XPath("//*[@id=\"content\"]/div/h4"));
           Assert.AreEqual("Welcome to the Secure Area. When you are done click logout below.", loginMessage.Text);
                
            driver.Quit();
        }
    }
}
